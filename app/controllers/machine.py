# -*- coding: utf-8 -*-
from flask import Blueprint, render_template

from app.models.machine import Machine

blueprint = Blueprint('machine', __name__)

@blueprint.route('/machine')
def machine():
    return render_template('home/index.html')
