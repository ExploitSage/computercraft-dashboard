# -*- coding: utf-8 -*-
from flask import Blueprint, render_template

from app.models.server import Server

blueprint = Blueprint('server', __name__)

@blueprint.route('/server')
def server():
    return render_template('home/index.html')
