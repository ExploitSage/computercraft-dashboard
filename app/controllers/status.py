# -*- coding: utf-8 -*-
from flask import Blueprint, render_template

from app.models.status import Status

blueprint = Blueprint('status', __name__)

@blueprint.route('/status')
def status():
    return render_template('home/index.html')
